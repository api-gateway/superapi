# 🚀 SuperAPI 🚀 - an awesome WSO2 API Manager CLI
You can now automate most of your API Gateway task using SuperAPI.
It features:
 - Create API
 - Publish API
 - Deprecate / Delete API
 - Create Application
 - Delete Application
 - Subscribe an App to an API
 - Fetch token

SuperAPI is cross-platform (Windows, MaxOS & Linux) program that reads NDJSON (Newline Delimited JSON) from its Stdin
processes the information and prints NDJSON to the Stdout.

## How to use it

## First provide your configuration file

for exemple put this into `./config.json`

```json
{
  "environments": [
    {
      "name": "stress",
      "serverletURL": "https://prd-apifull-1.cc.cec.eu.int:9443",
      "gatewayURL": "https://prd-apifull-1.cc.cec.eu.int:8243",
      "clientId": "MY_KEY",
      "clientSecret": "MY_SECRET"
    },
    {
      "name": "intra",
      "serverletURL": "https://intra-apifull-1.cc.cec.eu.int:9443",
      "gatewayURL": "https://prd-apifull-1.cc.cec.eu.int:8243",
      "clientId": "MY_KEY",
      "clientSecret": "MY_SECRET"
    },
	  {
      "name": "prod",
      "serverletURL": "https://apifull-extra-1.cc.cec.eu.int:9443",
      "gatewayURL": "https://apifull-extra-1.cc.cec.eu.int:8243",
      "clientId": "MY_KEY",
      "clientSecret": "MY_SECRET"
    }
  ]
}
```

## Then select the environement you want to use
The first parameter you will need to provide to `./superapi` will be your chosen environement

```
john@laptop:~/Code/superapi/$ ./superapi
Usage: superapi [options] [command]

Options:
  -h, --help                display help for command

Commands:
  stress [module] [action]
  intra [module] [action]
  prod [module] [action]
  help [command]            display help for command
```

there your can see that I have 3 environements `stress, `intra` & `prod`

To select prod:
```./superapi prod```

## Select the module to use with this environement

after typing `./superapi prod`
superapi will be listing you the modules that you can use with this environment

```
Usage: environment [options] [command]

Options:
  -h, --help      display help for command

Commands:
  api [actions]
  app [actions]
  help [command]  display help for command
```

Currently superapi has two module

- the `api` module
- the `app` module

## Using the api module

### Searching

the search could be based on these properties

- version
- context
- status
- description
- subcontext
- doc
- provider
- tag

#### Search examples :

- `./superapi stress api search name:*` will retreive all of your apis
- `./superapi prod api search name:test` will retreive only the api with a name that is start with test
- `./superapi acc api search x:y` will return the api with api that have the property x matching the y value
- `./superapi intra api search 'x:y a:b'` and your could either mixup the filters
- `./superapi intra api search  'context:/my_api/v1.0.0'` will retrieve version 1.0.0 pf api with context 'my_api'

### Creating

There you will have two options:

- passing the data as a njson into the stdin of superapi `cat apiData.json | ./superapi prod api create`
- or either using the `create [name] [context] [version] [url]` parameters

#### Examples:

- ```./superapi stress api create mySuperApi myContext v0.1 http://mywebsite.com```
- ```echo {"name":"mySuperApi", "context":"myCtx", "version":"v0.1", "endpointConfig":{"endpoint_type":"http","sandbox_endpoints":{"url":"oui.com"},"production_endpoints":{"url":"oui.com"}} > entry.json && cat entry.json | ./superapi stress api create```

### Publishing

```./superapi stress api search name:mySuperApi | ./superapi stress api publish```

### Deleting

#### Exemples:

- `./superapi stress api delete mySuperApi` delete mySuperApi
- `./superapi stress api search name:mySuperApi | ./superapi stress api delete` also delete mySuperApi

## Cloning by using intermediary file

```./superapi prod api search "compass" > apis_to_clone.json```
```cat apis_to_clone.json | ./superapi intra api publish```


## Cloning "compass" APIs from PROD to INTRA, by piping

```./superapi prod api search "compass" | ./superapi intra api create```

then delete apis in the first instance

```cat super1.json | ./superapi prod api delete```
```cat super2.json | ./superapi prod api delete```

that's all :)

## Using the Application (app) module

## Doc

### Environements

```
./superapi
Usage: superapi [options] [command]

Options:
  -h, --help                display help for command

Commands:
  stress [module] [action]
  intra [module] [action]
  prod [module] [action]
  help [command]            display help for command

```


### Application Actions

```
Usage: app [options] [command]

Options:
  -h, --help                        display help for command

Commands:
  search [search...]
  create [name] [throttlingPolicy]
  delete [appname]
  credentials
  token
  help [command]                    display help for command
```

#### Create application

```./superapi prod app create myApplication```

#### Search application and store to file
```./superapi prod app search 'myApplication' > my_apps.json```

#### Delete application
via parameter:

```./superapi prod app delete myApplication```

via stding:

```car my_apps.json | ./superapi prod app delete```

### Subscriptions

#### Subscribing an application to different APIs
Via stdin
Get the app

```./superapi prod app create myApplication #creates the APP```

Get the APIs you want to subscribe to

```./superapi prod api search "compass" > compass_apis.json```

Subscribe the app to the APIs

```cat compass_apis.json | ./superapi prod app subscribe myApplication```

### Mediation policies
### View the mediation policies of one ore more APIs
First search the APIs you want to work on, e.g. all APIs created by 'buckron' :
```./superapi prod api search "provider:buckron" > some_retrieved_apis.json```

Then pass this file to SuperAPI to list mediators in the console
```cat some_retrieved_apis.json > ./superapi prod api mediation list```

Or save them into a file
```cat some_retrieved_apis.json > ./superapi prod api mediation list > mediators.txt```

### Add a mediation policy to one or more APIs
Select one or more APIs you want to work on, e.g. all APIs created by 'buckron':
```./superapi prod api search "provider:buckron" > some_retrieved_apis.json```

Then pass this file to SuperAPI and indicate the filepath of the mediator to add (.xml) and the type [in|out]:
```cat some_retrieved_apis.json > ./superapi prod api mediation add in mediator.xml > add_operation_result.txt```

Note: this will iterate on the APIs contained in `some_retrieved_apis.json` and add a new mediator to it (In or Out), the results will be stored in the file `add_operation_result.txt`.

Note: 
Type 'in': In Sequence - entering the Gateway toward backend
Type 'out': Out Sequence - Going out from backend back to Gateway and to client

### Delete a mediation policy by ID from on or more APIs
Select one or more APIs you want to work on, e.g. all APIs created by 'buckron':
```./superapi prod api search "provider:buckron" > some_retrieved_apis.json```

Then pass this file to SuperAPI and indicate the mediator id to remove:
```cat some_retrieved_apis.json > ./superapi prod api mediation remove 01234567-0123-0123-0123-012345678901 > remove_operation_result.txt```

Note: this will iterate on the APIs contained in `some_retrieved_apis.json` and perform and mediator deletion on each, the results will be stored in the file `remove_operation_result.txt`.


### Delete all mediation policies from one or more APIs
Select one or more APIs you want to work on, e.g. all APIs created by 'buckron':
```./superapi prod api search "provider:buckron" > some_retrieved_apis.json```

Then pass this file to SuperAPI and indicate removal (by omitting any `id` parameter):
```cat some_retrieved_apis.json > ./superapi prod api mediation remove > remove_operation_result.txt```

Note: this will iterate on the APIs contained in `some_retrieved_apis.json` and perform and complete mediator deletion on each, the results will be stored in the file `remove_operation_result.txt`.

### Upcoming features 
 - Change headers
 - Generate keys


## How to build the superapi binary

simply type `npm install && npm run build` and wait for the ./superapi bin to appear :)

## Support

In case of any issue with this application or to request bug fix or new features, you can reach us:
  
  - DIGIT-APIGTW-SUPPORT@ec.europa.eu
  - Our Teams channel GRP-API GATEWAY

API Gateway Team.