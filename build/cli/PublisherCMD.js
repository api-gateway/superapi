"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = require("commander");
var Utils_1 = require("../Utils");
var PublisherCMD = new commander_1.Command("api");
/**
 * reads from STDIN
 * @returns a stream of NDJSON
 */
var jsonStdinToObject = function () { return __awaiter(void 0, void 0, void 0, function () {
    var stdin, objects;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, (0, Utils_1.readStdIn)()];
            case 1:
                stdin = _a.sent();
                objects = stdin.split("\n").filter(function (data) { return data != ""; });
                return [2 /*return*/, objects.map(function (json) { return JSON.parse(json); })];
        }
    });
}); };
/**
 * Search in the publisher
 * @returns: NDJSON APIs that match
 */
PublisherCMD.command("search")
    .arguments("[search...]")
    .action(function (search) { return __awaiter(void 0, void 0, void 0, function () {
    var client, results, _i, results_1, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                return [4 /*yield*/, client.search(search.join(" "))];
            case 1:
                results = _a.sent();
                // we format result in json /n
                for (_i = 0, results_1 = results; _i < results_1.length; _i++) {
                    result = results_1[_i];
                    console.log(JSON.stringify(result));
                }
                return [2 /*return*/];
        }
    });
}); });
/**
 * Reads NDJSON from STDIN to publish it
 * @returns: Created API NDJONS, or Error NDJSON
 */
PublisherCMD.command("create")
    .arguments("[name] [context] [version] [url]")
    .action(function (name, context, version, url) { return __awaiter(void 0, void 0, void 0, function () {
    var data, err_1, stdin, apis, _i, apis_1, _a, id, api, data, err_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                if (!name) return [3 /*break*/, 4];
                if (!context || !version || !url) {
                    console.log("syntaxe is [name] [context] [version] [url]");
                    return [2 /*return*/];
                }
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, global.client.create({
                        name: name,
                        context: context,
                        version: version,
                        endpointConfig: {
                            endpoint_type: "http",
                            sandbox_endpoints: { url: url },
                            production_endpoints: { url: url },
                        },
                        gatewayEnvironments: ["Production and Sandbox"],
                    }, false)];
            case 2:
                data = _b.sent();
                console.log(JSON.stringify(data));
                return [2 /*return*/];
            case 3:
                err_1 = _b.sent();
                if (err_1.reseponse) {
                    console.log(err_1.response.data);
                }
                else {
                    console.log(err_1);
                }
                return [3 /*break*/, 4];
            case 4: return [4 /*yield*/, (0, Utils_1.readStdIn)()];
            case 5:
                stdin = _b.sent();
                apis = stdin.split("\n").filter(function (data) { return data != ""; });
                // console.log(apis);
                apis = apis.map(function (json) { return JSON.parse(json); });
                _i = 0, apis_1 = apis;
                _b.label = 6;
            case 6:
                if (!(_i < apis_1.length)) return [3 /*break*/, 11];
                _a = apis_1[_i];
                id = _a.id, api = __rest(_a, ["id"]);
                _b.label = 7;
            case 7:
                _b.trys.push([7, 9, , 10]);
                return [4 /*yield*/, global.client.create(api, false)];
            case 8:
                data = _b.sent();
                console.log(JSON.stringify(data));
                return [3 /*break*/, 10];
            case 9:
                err_2 = _b.sent();
                if (err_2.reseponse) {
                    console.log(err_2.response.data);
                }
                else {
                    console.log(err_2);
                }
                return [3 /*break*/, 10];
            case 10:
                _i++;
                return [3 /*break*/, 6];
            case 11: return [2 /*return*/];
        }
    });
}); });
PublisherCMD.command("test <start> <stop> [offset]").action(function (start, stop, offset) { return __awaiter(void 0, void 0, void 0, function () {
    var client;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                return [4 /*yield*/, client.foundBugIndex("*", parseInt(start), parseInt(stop), parseInt(offset))];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
PublisherCMD.command("load [index]").action(function (index) { return __awaiter(void 0, void 0, void 0, function () {
    var client, result, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, client.getApiByResultIndex(parseInt(index))];
            case 2:
                result = _a.sent();
                console.log(result);
                return [3 /*break*/, 4];
            case 3:
                err_3 = _a.sent();
                process.stderr.write(JSON.stringify(err_3.response.data));
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
PublisherCMD.command("loadmini [index]").action(function (index) { return __awaiter(void 0, void 0, void 0, function () {
    var client, result, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, client.getApiByResultIndex(parseInt(index), false)];
            case 2:
                result = _a.sent();
                console.log(result);
                return [3 /*break*/, 4];
            case 3:
                err_4 = _a.sent();
                process.stderr.write(JSON.stringify(err_4.response.data));
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
/**
 * This function will delete an API
 * @params apiName: name of the API to delete, or the NDJSON object as input will be deleted
 * @returns nothing if successfull, the error otherwise
 */
PublisherCMD.command("delete [apiId]").action(function (apiId) { return __awaiter(void 0, void 0, void 0, function () {
    var res, apis, _i, apis_2, api, res, err_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 8, , 9]);
                if (!apiId) return [3 /*break*/, 2];
                return [4 /*yield*/, global.client.delete(apiId)];
            case 1:
                res = _a.sent();
                console.log(res || "deleted");
                return [2 /*return*/];
            case 2: return [4 /*yield*/, jsonStdinToObject()];
            case 3:
                apis = _a.sent();
                _i = 0, apis_2 = apis;
                _a.label = 4;
            case 4:
                if (!(_i < apis_2.length)) return [3 /*break*/, 7];
                api = apis_2[_i];
                return [4 /*yield*/, global.client.delete(api.id)];
            case 5:
                res = _a.sent();
                console.log(res || "deleted");
                _a.label = 6;
            case 6:
                _i++;
                return [3 /*break*/, 4];
            case 7: return [3 /*break*/, 9];
            case 8:
                err_5 = _a.sent();
                console.log(err_5.response.data);
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}); });
/**
 * This function remove mediator by id or all
 * @params id: id of the mediator to delete, or the NDJSON object as input will be deleted
 * @returns nothing if successfull, the error otherwise
 */
PublisherCMD.command("mediator_remove")
    .arguments("[id]")
    .action(function (id) { return __awaiter(void 0, void 0, void 0, function () {
    var client, apis, _i, apis_3, api, res, _a, apis_4, api, res, err_6;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                client = global.client;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 11, , 12]);
                return [4 /*yield*/, jsonStdinToObject()];
            case 2:
                apis = _b.sent();
                if (!!id) return [3 /*break*/, 6];
                _i = 0, apis_3 = apis;
                _b.label = 3;
            case 3:
                if (!(_i < apis_3.length)) return [3 /*break*/, 6];
                api = apis_3[_i];
                return [4 /*yield*/, client.removeAllMediators(api)];
            case 4:
                res = _b.sent();
                _b.label = 5;
            case 5:
                _i++;
                return [3 /*break*/, 3];
            case 6:
                if (!id) return [3 /*break*/, 10];
                _a = 0, apis_4 = apis;
                _b.label = 7;
            case 7:
                if (!(_a < apis_4.length)) return [3 /*break*/, 10];
                api = apis_4[_a];
                return [4 /*yield*/, client.removeMediatorByID(api, id)];
            case 8:
                res = _b.sent();
                console.log(res || "mediator deleted");
                _b.label = 9;
            case 9:
                _a++;
                return [3 /*break*/, 7];
            case 10: return [3 /*break*/, 12];
            case 11:
                err_6 = _b.sent();
                console.log(err_6.response.data);
                return [3 /*break*/, 12];
            case 12: return [2 /*return*/];
        }
    });
}); });
/**
* This function add mediator file and type
* @params type: IN sequence or OUT sequence
* @returns nothing if successfull, the error otherwise
*/
PublisherCMD.command("mediator_add")
    .arguments("[type] [filename]")
    .action(function (type, filename) { return __awaiter(void 0, void 0, void 0, function () {
    var client, apis, _i, apis_5, api, res, e_1, err_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 9, , 10]);
                if (!type) {
                    console.log("syntaxe is `mediator add [in|out] [filename.xml]`");
                    return [2 /*return*/];
                }
                if (!filename) {
                    console.log("syntaxe is mediator add ".concat(type, " [filename.xml]"));
                    return [2 /*return*/];
                }
                return [4 /*yield*/, jsonStdinToObject()];
            case 2:
                apis = _a.sent();
                _i = 0, apis_5 = apis;
                _a.label = 3;
            case 3:
                if (!(_i < apis_5.length)) return [3 /*break*/, 8];
                api = apis_5[_i];
                _a.label = 4;
            case 4:
                _a.trys.push([4, 6, , 7]);
                return [4 /*yield*/, client.addMediatorByFileName(api, type, filename)];
            case 5:
                res = _a.sent();
                console.log("mediator added" + JSON.stringify(res));
                return [3 /*break*/, 7];
            case 6:
                e_1 = _a.sent();
                console.log("failed to add mediator" + e_1);
                return [3 /*break*/, 7];
            case 7:
                _i++;
                return [3 /*break*/, 3];
            case 8: return [3 /*break*/, 10];
            case 9:
                err_7 = _a.sent();
                console.log(err_7.response.data);
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); });
/**
* This function returns all available mediators
* @returns array of mediator, empty array
*/
PublisherCMD.command("mediator_list")
    .action(function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, apis, _i, apis_6, api, res, err_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 7, , 8]);
                return [4 /*yield*/, jsonStdinToObject()];
            case 2:
                apis = _a.sent();
                _i = 0, apis_6 = apis;
                _a.label = 3;
            case 3:
                if (!(_i < apis_6.length)) return [3 /*break*/, 6];
                api = apis_6[_i];
                return [4 /*yield*/, client.getMediatorsFromAPI(api.id)];
            case 4:
                res = _a.sent();
                console.log(res);
                _a.label = 5;
            case 5:
                _i++;
                return [3 /*break*/, 3];
            case 6: return [3 /*break*/, 8];
            case 7:
                err_8 = _a.sent();
                console.log(err_8.response.data);
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
/**
 * Get a token from the API Gateway using the credentials in the config.json file
 */
PublisherCMD.command("token").action(function () { return __awaiter(void 0, void 0, void 0, function () {
    var token;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, global.client.getToken()];
            case 1:
                token = _a.sent();
                console.log(token);
                return [2 /*return*/];
        }
    });
}); });
/**
 * Publishes an API from the NDJSON input, the user running the script being the new provider
 * @params NDJSON of source API from STDIN, or else the params as [name] [context] [version] [url]
 * @returns NDJSON of new API, or error message
 */
PublisherCMD.command("publish")
    .arguments("[name] [context] [version] [url]")
    .action(function (name, context, version, url) { return __awaiter(void 0, void 0, void 0, function () {
    var client, data, stdin, apis, _i, apis_7, _a, id, api, data, err_9;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                client = global.client;
                if (!name) return [3 /*break*/, 2];
                if (!context || !version || !url) {
                    console.log("syntaxe is [name] [context] [version] [url]");
                    return [2 /*return*/];
                }
                return [4 /*yield*/, client.create({
                        name: name,
                        context: context,
                        version: version,
                        endpointConfig: {
                            endpoint_type: "http",
                            sandbox_endpoints: { url: url },
                            production_endpoints: { url: url },
                        },
                        gatewayEnvironments: ["Production and Sandbox"],
                    }, true)];
            case 1:
                data = _b.sent();
                console.log(JSON.stringify(data));
                return [2 /*return*/];
            case 2: return [4 /*yield*/, (0, Utils_1.readStdIn)()];
            case 3:
                stdin = _b.sent();
                apis = stdin.split("\n").filter(function (data) { return data != ""; });
                apis = apis.map(function (json) { return JSON.parse(json); });
                _i = 0, apis_7 = apis;
                _b.label = 4;
            case 4:
                if (!(_i < apis_7.length)) return [3 /*break*/, 11];
                _a = apis_7[_i];
                id = _a.id, api = __rest(_a, ["id"]);
                _b.label = 5;
            case 5:
                _b.trys.push([5, 9, , 10]);
                return [4 /*yield*/, client.create(api, true)];
            case 6:
                data = _b.sent();
                if (!(data.code == 400 && data.description.includes("Error occurred while adding the API. A duplicate API context already exists for"))) return [3 /*break*/, 8];
                return [4 /*yield*/, client.createNewVersion(api, true)];
            case 7:
                // console.log("Creating new version from existing api");
                data = _b.sent();
                _b.label = 8;
            case 8:
                console.log(JSON.stringify(data));
                return [3 /*break*/, 10];
            case 9:
                err_9 = _b.sent();
                if (err_9.reseponse) {
                    // console.log(err.response.data);
                    console.log("Creating new version");
                }
                else {
                    // console.log(err);
                    console.log("Creating new version");
                }
                return [3 /*break*/, 10];
            case 10:
                _i++;
                return [3 /*break*/, 4];
            case 11: return [2 /*return*/];
        }
    });
}); });
/**
 * Will change the lifecycle of the API to deprecated
 * @params the NDJSON as STDIN or id as param
 * @returnq the NDJSON of the API object, updated, or the error message
 */
PublisherCMD.command("deprecate [appId]").action(function (appId) { return __awaiter(void 0, void 0, void 0, function () {
    var client, stdin, apis, _i, apis_8, api, data, err_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = global.client;
                if (!appId) return [3 /*break*/, 2];
                return [4 /*yield*/, client.deprecate(appId)];
            case 1: return [2 /*return*/, _a.sent()];
            case 2: return [4 /*yield*/, (0, Utils_1.readStdIn)()];
            case 3:
                stdin = _a.sent();
                apis = stdin.split("\n").filter(function (data) { return data != ""; });
                // console.log({apis})
                apis = apis.map(function (json) { return JSON.parse(json); });
                _i = 0, apis_8 = apis;
                _a.label = 4;
            case 4:
                if (!(_i < apis_8.length)) return [3 /*break*/, 9];
                api = apis_8[_i];
                _a.label = 5;
            case 5:
                _a.trys.push([5, 7, , 8]);
                return [4 /*yield*/, client.deprecate(api)];
            case 6:
                data = _a.sent();
                console.log(JSON.stringify(data));
                return [3 /*break*/, 8];
            case 7:
                err_10 = _a.sent();
                if (err_10.reseponse) {
                    console.log(err_10.response.data);
                }
                else {
                    console.log(err_10);
                }
                return [3 /*break*/, 8];
            case 8:
                _i++;
                return [3 /*break*/, 4];
            case 9: return [2 /*return*/];
        }
    });
}); });
exports.default = PublisherCMD;
