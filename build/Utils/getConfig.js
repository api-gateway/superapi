"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
/** Get config from file in same directory */
var configPath = process.cwd() + "/config.json";
// @todo: get config from ENV variables
var createConfig = function () {
    var template = {
        environments: [
            {
                name: "acceptance",
                serverletURL: "https://apifull-exacc-1.cc.cec.eu.int:9443",
                gatewayURL: "https://apifull-exacc-1.cc.cec.eu.int:8243",
                clientId: "",
                clientSecret: "",
            },
            {
                name: "intra",
                serverletURL: "https://apifull-intra-1.cc.cec.eu.int:9443",
                gatewayURL: "https://apifull-intra-1.cc.cec.eu.int:8243",
                clientId: "",
                clientSecret: "",
            },
            {
                name: "prod",
                serverletURL: "https://apifull-extra-1.cc.cec.eu.int:9443",
                gatewayURL: "https://apifull-extra-1.cc.cec.eu.int:8243",
                clientId: "",
                clientSecret: "",
            },
            {
                name: "stress",
                serverletURL: "https://prd-apifull-1.cc.cec.eu.int:9443",
                gatewayURL: "https://prd-apifull-1.cc.cec.eu.int:8243",
                clientId: "",
                clientSecret: "",
            },
        ],
    };
    fs_1.default.writeFileSync(configPath, JSON.stringify(template, null, 4));
};
var getConfig = function () {
    if (!fs_1.default.existsSync(configPath)) {
        createConfig();
        console.log("no config file was found default template has been created");
        process.exit();
    }
    var configStr = fs_1.default.readFileSync(configPath).toString();
    var config = JSON.parse(configStr);
    return config;
};
exports.default = getConfig;
