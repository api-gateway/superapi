import { readStdIn } from "../Utils";
import AppsAPIClient from "../APIMBot/AppsAPIClient";

const { Command } = require("commander");

// @todo - move to utils
const jsonStdinToObject = async () => {
  const stdin = await readStdIn();
  let objects: any[] = stdin.split("\n").filter((data) => data != "");

  return objects.map((json) => JSON.parse(json));
};

const AppsCMD = new Command("app");

/**
 * This function makes a search on all APIs
 * @params the input to search from
 * @return a stream of NDJSON with corresponding APIs
 */
AppsCMD.command("search")
  .arguments("[search...]")
  .action(async (search: string[]) => {
    const results = await global.client.search(search.join(" "));

    // we format result in json /n
    for (let result of results) {
      console.log(JSON.stringify(result));
    }
  });

/**
 * Creates an API
 * @params api from NDJSON or [name] [context] [version] [url]
 * @returns the created API as NDJSON or the error message
 */
AppsCMD.command("create")
  .arguments("[name] [throttlingPolicy]")
  .action(async (name?: string, throttlingPolicy?: string) => {
    if (name) {
      try {
        const res = await global.client.create({
          name,
          throttlingPolicy: throttlingPolicy || "Unlimited",
        });
        console.log(JSON.stringify(res));
        // console.log(JSON.stringify());
      } catch (err: any) {
        console.log(err.response.data);
      }
      return;
    }

    // récupérer ce qu'il y a sur stdin
    const stdin = await readStdIn();
    let apps: any[] = stdin.split("\n").filter((data) => data != "");

    // console.log(apps);
    apps = apps.map((json) => JSON.parse(json));
    // récupérer ce qu'il y a sur stdin

    for (let { id, ...app } of apps) {
      // console.log(app.name);
      const newApp = await global.client.create(app);
      console.log(JSON.stringify(newApp));
    }
  });


/**
 * This function deletes an API
 * @params the NDJSON of the API to delete or the API id as parameter
 * @returns deleted message if successfull, error message otherwise
 */
AppsCMD.command("delete")
  .arguments("[appname]")
  .action(async (appName: string) => {
    try {
      // pas de nom on lit depius le stdin
      if (!appName) {
        const stdin = await readStdIn();
        let apps: any[] = stdin.split("\n").filter((data) => data != "");

        // console.log(apps);
        apps = apps.map((json) => JSON.parse(json));
        for (let app of apps) {
          const res = await global.client.delete(app.id);
          console.log(res || "deleted");
        }

        return;
      }

      const res = await global.client.deleteByName(appName);
      console.log(res || "deleted");
    } catch (err: any) {
      console.log(err.toString());
    }
  });

/**
 * This returns a t
 */
AppsCMD.command("credentials").action(async () => {
  const stdin = await readStdIn();
  let apps: any[] = stdin.split("\n").filter((data) => data != "");

  // console.log(apps);
  apps = apps.map((json) => JSON.parse(json));

  for (let { id, ...app } of apps) {
    const newApp = await (global.client as AppsAPIClient).getCredentials(
      app,
      "PRODUCTION"
    );
    console.log(JSON.stringify(newApp));
  }
});

AppsCMD.command("token").action(async () => {
  const token = await global.client.getToken();
  console.log(token);
});


AppsCMD.command("generate-credentials <appId> <type>").action(async (appId: string, type) => {
  const client = global.client as AppsAPIClient;
  try {
    const creds = await client.generateCredentials(appId, type);
    console.log(creds)
  } catch (err) {
    console.log(err.response.data)
  }
})

AppsCMD.command("subscribe")
  .arguments("appName")
  .action(async (appName) => {
    const client = global.client as AppsAPIClient;
    const apps = await client.search(appName);

    if (apps.length == 0) {
      return console.log("no matching app found");
    }

    const apis = await jsonStdinToObject();
    client.subscribe(apps[0], apis);
  });

export default AppsCMD;
