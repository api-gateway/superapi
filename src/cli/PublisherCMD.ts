import { Command } from "commander";
import { readStdIn } from "../Utils";
import PublisherAPIClient from "../APIMBot/PublisherAPIClient";

const PublisherCMD = new Command("api");

/**
 * reads from STDIN
 * @returns a stream of NDJSON
 */
const jsonStdinToObject = async () => {
  const stdin = await readStdIn();
  let objects: any[] = stdin.split("\n").filter((data) => data != "");

  return objects.map((json) => JSON.parse(json));
};


/**
 * Search in the publisher
 * @returns: NDJSON APIs that match
 */
PublisherCMD.command("search")
  .arguments("[search...]")
  .action(async (search: string[]) => {
    const client = global.client as PublisherAPIClient;
    const results = await client.search(search.join(" "));

    // we format result in json /n
    for (let result of results) {
      console.log(JSON.stringify(result));
    }
  });

/**
 * Reads NDJSON from STDIN to publish it
 * @returns: Created API NDJONS, or Error NDJSON
 */
PublisherCMD.command("create")
  .arguments("[name] [context] [version] [url]")
  .action(
    async (name?: string, context?: string, version?: string, url?: string) => {

      if (name) {
        if (!context || !version || !url) {
          console.log("syntaxe is [name] [context] [version] [url]");
          return;
        }

        try {
          const data = await global.client.create(
            {
              name,
              context,
              version,
              endpointConfig: {
                endpoint_type: "http",
                sandbox_endpoints: { url },
                production_endpoints: { url },
              },
              gatewayEnvironments: ["Production and Sandbox"],
            },
            false
          );
          
          console.log(JSON.stringify(data));
          return;
        } catch(err: any) {
          if (err.reseponse) {
            console.log(err.response.data);
          } else {
            console.log(err);
          }
        }
      }

      //From stdin
      const stdin = await readStdIn();
      let apis: any[] = stdin.split("\n").filter((data) => data != "");

      // console.log(apis);
      apis = apis.map((json) => JSON.parse(json));

      for (let { id, ...api } of apis) {
        try {
          const data = await global.client.create(api, false);
          console.log(JSON.stringify(data));
        } catch (err: any) {
          if (err.reseponse) {
            console.log(err.response.data);
          } else {
            console.log(err);
          }
        }
      }
    }
  );


PublisherCMD.command("test <start> <stop> [offset]").action(async (start, stop, offset) => {
  const client = global.client as PublisherAPIClient;

  await client.foundBugIndex("*", parseInt(start), parseInt(stop), parseInt(offset))

})
PublisherCMD.command("load [index]").action(async (index) => {
  const client = global.client as PublisherAPIClient;
  try {
    const result = await client.getApiByResultIndex(parseInt(index))
    console.log(result)
  } catch (err) {
    process.stderr.write(JSON.stringify(err.response.data))
  }
})
PublisherCMD.command("loadmini [index]").action(async (index) => {
  const client = global.client as PublisherAPIClient;
  try {
    const result = await client.getApiByResultIndex(parseInt(index), false)
    console.log(result)
  } catch (err) {
    process.stderr.write(JSON.stringify(err.response.data))
  }
})

/**
 * This function will delete an API
 * @params apiName: name of the API to delete, or the NDJSON object as input will be deleted
 * @returns nothing if successfull, the error otherwise
 */
PublisherCMD.command("delete [apiId]").action(async (apiId) => {
  try {
    if (apiId) {
      const res = await global.client.delete(apiId);
      console.log(res || "deleted");
      return
    }


    const apis = await jsonStdinToObject()

    for (let api of apis) {
      const res = await global.client.delete(api.id);
      console.log(res || "deleted");
    }

  } catch (err: any) {
    console.log(err.response.data);
  }
});


/**
 * This function remove mediator by id or all
 * @params id: id of the mediator to delete, or the NDJSON object as input will be deleted
 * @returns nothing if successfull, the error otherwise
 */
PublisherCMD.command("mediator_remove")
  .arguments("[id]")
  .action(async (id?: string) => {
    const client = global.client as PublisherAPIClient;
    try {
      const apis = await jsonStdinToObject()

      if(!id) {
        for (let api of apis) {
          const res = await client.removeAllMediators(api);
          // console.log(res || "mediator deleted");
        }
      }

      if(id) {
        for (let api of apis) {
          const res = await client.removeMediatorByID(api, id);
          console.log(res || "mediator deleted");
        }
      }

    } catch (err: any) {
      console.log(err.response.data);
    }
  });


  /**
 * This function add mediator file and type
 * @params type: IN sequence or OUT sequence
 * @returns nothing if successfull, the error otherwise
 */
PublisherCMD.command("mediator_add")
.arguments("[type] [filename]")
.action(async (type?: string, filename?: string) => {
  const client = global.client as PublisherAPIClient;
  try {
    if (!type) {
      console.log("syntaxe is `mediator add [in|out] [filename.xml]`")
      return
    }
    if(!filename) {
      console.log(`syntaxe is mediator add ${type} [filename.xml]`)
      return
    }

    const apis = await jsonStdinToObject()

    for (let api of apis) {
      try {
        const res = await client.addMediatorByFileName(api, type, filename);
        console.log("mediator added" + JSON.stringify(res));
      } catch(e) {
        console.log("failed to add mediator" + e);
      }
    }


  } catch (err: any) {
    console.log(err.response.data);
  }
});

  /**
 * This function returns all available mediators
 * @returns array of mediator, empty array
 */
  PublisherCMD.command("mediator_list")
  .action(async () => {
    const client = global.client as PublisherAPIClient;
    try {  
      const apis = await jsonStdinToObject()
  
      for (let api of apis) {
        const res = await client.getMediatorsFromAPI(api.id);
        console.log(res);
      }
  
    } catch (err: any) {
      console.log(err.response.data);
    }
  });

/**
 * Get a token from the API Gateway using the credentials in the config.json file
 */
PublisherCMD.command("token").action(async () => {
  const token = await global.client.getToken();
  console.log(token);
});


/**
 * Publishes an API from the NDJSON input, the user running the script being the new provider
 * @params NDJSON of source API from STDIN, or else the params as [name] [context] [version] [url]
 * @returns NDJSON of new API, or error message
 */
PublisherCMD.command("publish")
  .arguments("[name] [context] [version] [url]")
  .action(
    async (name?: string, context?: string, version?: string, url?: string) => {
      const client = global.client as PublisherAPIClient;
      //////////////////////////////
      // * en ligne de commande * //
      //////////////////////////////
      // ./superapi prod api publish my_api context version url
      if (name) {
        if (!context || !version || !url) {
          console.log("syntaxe is [name] [context] [version] [url]");
          return;
        }

        const data = await client.create(
          {
            name,
            context,
            version,
            endpointConfig: {
              endpoint_type: "http",
              sandbox_endpoints: { url },
              production_endpoints: { url },
            },
            gatewayEnvironments: ["Production and Sandbox"],
          },
          true
        );
        console.log(JSON.stringify(data));
        return;
      }

      //From STDIN
      // cat some_apis.json | ./superapi prod publish
      const stdin = await readStdIn();
      let apis: any[] = stdin.split("\n").filter((data) => data != "");

      apis = apis.map((json) => JSON.parse(json));

      for (let { id, ...api } of apis) {
        try {
          let data = await client.create(api, true);
          //Making sure there wasn't the same existing context but with another version
          // console.log(data.code)
          if(data.code == 400 && data.description.includes("Error occurred while adding the API. A duplicate API context already exists for")) {
            // console.log("Creating new version from existing api");
            data = await client.createNewVersion(api, true);
          }
          console.log(JSON.stringify(data));
        } catch (err: any) {
          if (err.reseponse) {
            // console.log(err.response.data);
            console.log("Creating new version")
          } else {
            // console.log(err);
            console.log("Creating new version")
          }
        }
      }
    }
  );

/**
 * Will change the lifecycle of the API to deprecated
 * @params the NDJSON as STDIN or id as param
 * @returnq the NDJSON of the API object, updated, or the error message
 */
PublisherCMD.command("deprecate [appId]").action(
  async (appId: string) => {
    const client = global.client as PublisherAPIClient;

    if (appId) {
      return await client.deprecate(appId);
    }

    const stdin = await readStdIn();
    // console.log({stdin})
    let apis: any[] = stdin.split("\n").filter((data) => data != "");
    // console.log({apis})
    apis = apis.map((json) => JSON.parse(json));
    // console.log({apis})

    for (let api of apis) {
      try {
        const data = await client.deprecate(api);
        console.log(JSON.stringify(data));
      } catch (err: any) {
        if (err.reseponse) {
          console.log(err.response.data);
        } else {
          console.log(err);
        }
      }
    }
  }
);

export default PublisherCMD;
