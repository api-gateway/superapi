/**
 * This function reads from the standard input and process any input
 */
const readStdIn = (): Promise<string> =>
  new Promise((res) => {
    process.stdin.resume();
    process.stdin.setEncoding("utf8");

    let output = "";

    process.stdin.on("data", (chunk) => {
      output += chunk;
    });

    process.stdin.on("end", () => {
      res(output);
    });
  });

export default readStdIn;
