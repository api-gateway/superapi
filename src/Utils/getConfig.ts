import fs from "fs";
import path from "path";

import { Config } from "../types/Config";

/** Get config from file in same directory */
const configPath = process.cwd() + "/config.json";

// @todo: get config from ENV variables

const createConfig = () => {
  const template = {
    environments: [
      {
        name: "acceptance",
        serverletURL: "https://apifull-exacc-1.cc.cec.eu.int:9443",
        gatewayURL: "https://apifull-exacc-1.cc.cec.eu.int:8243",
        clientId: "",
        clientSecret: "",
      },
      {
        name: "intra",
        serverletURL: "https://apifull-intra-1.cc.cec.eu.int:9443",
        gatewayURL: "https://apifull-intra-1.cc.cec.eu.int:8243",
        clientId: "",
        clientSecret: "",
      },
      {
        name: "prod",
        serverletURL: "https://apifull-extra-1.cc.cec.eu.int:9443",
        gatewayURL: "https://apifull-extra-1.cc.cec.eu.int:8243",
        clientId: "",
        clientSecret: "",
      },
      {
        name: "stress",
        serverletURL: "https://prd-apifull-1.cc.cec.eu.int:9443",
        gatewayURL: "https://prd-apifull-1.cc.cec.eu.int:8243",
        clientId: "",
        clientSecret: "",
      },
    ],
  };

  fs.writeFileSync(configPath, JSON.stringify(template, null, 4));
};

const getConfig = (): Config => {
  if (!fs.existsSync(configPath)) {
    createConfig();
    console.log("no config file was found default template has been created");
    process.exit();
  }
  const configStr = fs.readFileSync(configPath).toString();
  const config: Config = JSON.parse(configStr);
  return config;
};

export default getConfig;
